FROM golang

ADD . /go/src/gitlab.com/triglav86/gotask
WORKDIR /go/src/gitlab.com/triglav86/gotask
RUN go get ./

RUN go install gitlab.com/triglav86/gotask

ENTRYPOINT /go/bin/gotask

EXPOSE 7777