# Тестовая задача для Golang разработчика


## Задача

- Форкнуть репозиторий
- Написать сервис на Golang, который принимает массив URL-ов в теле, 
для данных URL он должен загрузить инф. о кол-во тегов на странице, код ответа, 
и все заголовки, пример ответа:

``` json
[
  {
    "url": "http://www.example.com/",
    "meta": {
      "status": 200,
      
      "headers": [
        {
          "content-type": "text\/html",
          "server": "nginx",
          "content-length": 605,
          "connection": "close",
          // ...
        }
      ]
    },
    "elemets": [
      {
        "tag-name": "html",
        "count": 1
      },
      {
        "tag-name": "head",
        "count": 1
      },
      // ...
    ]
  },
  // ...
]
```
- Сервис необходимо завернуть в Docker


## Решение

##### Запуск:

``git clone https://gitlab.com/triglav86/gotask.git``

``cd gotask``

``docker build -t gotask .``

``docker run --publish 7777:7777 --name gotask --rm gotask``

##### Пример:

Запрос:

``curl -X POST --data "[\"http://yandex.ru/\", \"http://habrahabr.ru/\"]" http://localhost:7777/stat-urls``

Ответ:

``` json
[
  {
    "url": "http://habrahabr.ru/",
    "meta": {
      "status": 200,
      "headers": {
        "Connection": "keep-alive",
        "Content-Type": "text/html; charset=UTF-8",
        "Date": "Mon, 01 Jan 2018 15:43:37 GMT",
        "Keep-Alive": "timeout=15",
        "P3p": "CP=\"CAO DSP COR CURa ADMa DEVa PSAa PSDa IVAi IVDi CONi OUR OTRi IND PHY ONL UNI FIN COM NAV INT DEM STA\"",
        "Public-Key-Pins": "pin-sha256=\"klO23nT2ehFDXCfx3eHTDRESMz3asj1muO+4aIdjiuY=\";pin-sha256=\"kUh5F9diW5KlrhQ+nEKTIVFWVZuNbVqkKtm+KOGPXCE=\"; max-age=15552000; includeSubDomains",
        "Server": "QRATOR",
        "Set-Cookie": "feed_flow=top; expires=Thu, 01-Feb-2018 15:43:37 GMT; Max-Age=2678400; path=/; httponly",
        "Strict-Transport-Security": "max-age=31536000",
        "Vary": "Accept-Encoding",
        "X-Content-Type-Options": "nosniff",
        "X-Engine": "engine-slave",
        "X-Frame-Options": "SAMEORIGIN",
        "X-Powered-By": "PHP/5.6.20-1+deb.sury.org~trusty+1"
      }
    },
    "elements": [
      {
        "tag-name": "h2",
        "count": 5
      },
      {
        "tag-name": "strong",
        "count": 1
      },
      {
        "tag-name": "stop",
        "count": 17
      },
      {
        "tag-name": "link",
        "count": 11
      },
      {
        "tag-name": "style",
        "count": 1
      },
      {
        "tag-name": "svg",
        "count": 152
      },
      {
        "tag-name": "button",
        "count": 19
      },
      {
        "tag-name": "section",
        "count": 2
      },
      {
        "tag-name": "",
        "count": 1
      },
      {
        "tag-name": "header",
        "count": 5
      },
      {
        "tag-name": "html",
        "count": 1
      },
      {
        "tag-name": "head",
        "count": 1
      },
      {
        "tag-name": "div",
        "count": 141
      },
      {
        "tag-name": "p",
        "count": 1
      },
      {
        "tag-name": "code",
        "count": 1
      },
      {
        "tag-name": "h3",
        "count": 5
      },
      {
        "tag-name": "meta",
        "count": 20
      },
      {
        "tag-name": "ul",
        "count": 33
      },
      {
        "tag-name": "li",
        "count": 124
      },
      {
        "tag-name": "input",
        "count": 3
      },
      {
        "tag-name": "article",
        "count": 5
      },
      {
        "tag-name": "lineargradient",
        "count": 4
      },
      {
        "tag-name": "noscript",
        "count": 2
      },
      {
        "tag-name": "title",
        "count": 2
      },
      {
        "tag-name": "img",
        "count": 10
      },
      {
        "tag-name": "footer",
        "count": 5
      },
      {
        "tag-name": "i",
        "count": 5
      },
      {
        "tag-name": "pre",
        "count": 1
      },
      {
        "tag-name": "script",
        "count": 23
      },
      {
        "tag-name": "body",
        "count": 1
      },
      {
        "tag-name": "a",
        "count": 181
      },
      {
        "tag-name": "path",
        "count": 165
      },
      {
        "tag-name": "form",
        "count": 1
      },
      {
        "tag-name": "span",
        "count": 390
      },
      {
        "tag-name": "label",
        "count": 3
      },
      {
        "tag-name": "br",
        "count": 31
      },
      {
        "tag-name": "g",
        "count": 1
      }
    ]
  },
  {
    "url": "http://yandex.ru/",
    "meta": {
      "status": 200,
      "headers": {
        "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate",
        "Content-Security-Policy": "connect-src 'self' wss://webasr.yandex.net https://mc.webvisor.com https://mc.webvisor.org wss://push.yandex.ru wss://portal-xiva.yandex.net https://yastatic.net https://home.yastatic.net https://yandex.ru https://*.yandex.ru static.yandex.sx brotli.yastatic.net et.yastatic.net portal-xiva.yandex.net yastatic.net home.yastatic.net yandex.ru *.yandex.ru *.yandex.net yandex.st; default-src 'self' blob: wss://portal-xiva.yandex.net yastatic.net portal-xiva.yandex.net; font-src 'self' https://yastatic.net static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net; frame-src 'self' yabrowser: data: https://ok.ru https://www.youtube.com https://player.video.yandex.net https://yastatic.net https://yandex.ru https://*.yandex.ru wfarm.yandex.net yastatic.net yandex.ru *.yandex.ru awaps.yandex.net *.cdn.yandex.net; img-src 'self' data: https://yastatic.net https://home.yastatic.net https://*.yandex.ru https://*.yandex.net https://*.tns-counter.ru awaps.yandex.net *.yastatic.net gdeua.hit.gemius.pl pa.tns-ua.com mc.yandex.com mc.webvisor.com mc.webvisor.org static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net yandex.ru *.yandex.ru *.yandex.net *.tns-counter.ru *.gemius.pl yandex.st; media-src 'self' blob: data: *.storage.yandex.net yastatic.net kiks.yandex.ru strm.yandex.ru; object-src 'self' *.yandex.net music.yandex.ru strm.yandex.ru yastatic.net kiks.yandex.ru awaps.yandex.net storage.mds.yandex.net; report-uri https://csp.yandex.net/csp?from=big.ru&showid=1514821417.23276.22876.30002&h=f62&yandexuid=6629809231514821417; script-src 'self' 'unsafe-inline' 'unsafe-eval' blob: https://suburban-widget.rasp.yandex.ru https://suburban-widget.rasp.yandex.net https://music.yandex.ru https://mc.yandex.fr https://mc.webvisor.com https://yandex.fr https://mc.webvisor.org https://yastatic.net https://home.yastatic.net https://mc.yandex.ru https://pass.yandex.ru www.youtube.com *.ytimg.com an.yandex.ru api-maps.yandex.ru static.yandex.sx webasr.yandex.net brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net yandex.ru www.yandex.ru mc.yandex.ru suggest.yandex.ru clck.yandex.ru awaps.yandex.net; style-src 'self' 'unsafe-inline' https://yastatic.net https://home.yastatic.net static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net;",
        "Content-Type": "text/html; charset=UTF-8",
        "Date": "Mon, 01 Jan 2018 15:43:37 GMT",
        "Expires": "Mon, 01 Jan 2018 15:43:37 GMT",
        "Last-Modified": "Mon, 01 Jan 2018 15:43:37 GMT",
        "P3p": "policyref=\"/w3c/p3p.xml\", CP=\"NON DSP ADM DEV PSD IVDo OUR IND STP PHY PRE NAV UNI\"",
        "Set-Cookie": "yp=1517413417.ygu.1; Expires=Thu, 30-Dec-2027 15:43:37 GMT; Domain=.yandex.ru; Path=/",
        "X-Content-Type-Options": "nosniff",
        "X-Frame-Options": "DENY",
        "X-Xss-Protection": "1; mode=block"
      }
    },
    "elements": [
      {
        "tag-name": "",
        "count": 1
      },
      {
        "tag-name": "a",
        "count": 129
      },
      {
        "tag-name": "dqs",
        "count": 35
      },
      {
        "tag-name": "ftgr",
        "count": 24
      },
      {
        "tag-name": "script",
        "count": 19
      },
      {
        "tag-name": "dbgrsae",
        "count": 32
      },
      {
        "tag-name": "noscript",
        "count": 4
      },
      {
        "tag-name": "i",
        "count": 14
      },
      {
        "tag-name": "h1",
        "count": 11
      },
      {
        "tag-name": "label",
        "count": 3
      },
      {
        "tag-name": "head",
        "count": 1
      },
      {
        "tag-name": "title",
        "count": 1
      },
      {
        "tag-name": "link",
        "count": 11
      },
      {
        "tag-name": "body",
        "count": 1
      },
      {
        "tag-name": "div",
        "count": 268
      },
      {
        "tag-name": "input",
        "count": 6
      },
      {
        "tag-name": "edvrt",
        "count": 29
      },
      {
        "tag-name": "fgn",
        "count": 31
      },
      {
        "tag-name": "ul",
        "count": 9
      },
      {
        "tag-name": "eflll",
        "count": 36
      },
      {
        "tag-name": "path",
        "count": 1
      },
      {
        "tag-name": "html",
        "count": 1
      },
      {
        "tag-name": "meta",
        "count": 15
      },
      {
        "tag-name": "style",
        "count": 2
      },
      {
        "tag-name": "fwap",
        "count": 43
      },
      {
        "tag-name": "dhtaq",
        "count": 30
      },
      {
        "tag-name": "span",
        "count": 116
      },
      {
        "tag-name": "button",
        "count": 3
      },
      {
        "tag-name": "aqwf",
        "count": 33
      },
      {
        "tag-name": "img",
        "count": 5
      },
      {
        "tag-name": "svg",
        "count": 1
      },
      {
        "tag-name": "form",
        "count": 2
      },
      {
        "tag-name": "fdpprt",
        "count": 34
      },
      {
        "tag-name": "ol",
        "count": 4
      },
      {
        "tag-name": "li",
        "count": 56
      },
      {
        "tag-name": "pjtr",
        "count": 33
      }
    ]
  }
]
```