package main

import (
	"log"
	"github.com/gorilla/mux"
	"net/http"
)

// Entry point of app
func main() {
	// create new router
	router := mux.NewRouter()

	// add handler for stat method
	router.HandleFunc("/stat-urls", RequestStat).Methods("POST")

	// start listening of port
	log.Fatal(http.ListenAndServe("0.0.0.0:7777", router))
}