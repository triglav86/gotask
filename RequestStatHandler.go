package main

import (
	"encoding/json"
	"net/http"
	"net/url"
	"io/ioutil"
	"strings"
	"golang.org/x/net/html"
)

// Handler for stat method
func RequestStat(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var urls []string
	var validUrls []string

	// Decode requested url list
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&urls)

	// Requested urls filtering, exclude not valid urls
	for _, u := range urls {
		_, err := url.ParseRequestURI(u)
		if err == nil {
			validUrls = append(validUrls, u)
		}
	}

	channels  := make(chan StatResponse, len(urls))
	responses := make([]StatResponse, 0)

	// loop through valid url list and request each in separate channel
	for _, u := range validUrls {
		go func(u string) {
			// request url and read body
			resp, _ := http.Get(u)
			body, _ := ioutil.ReadAll(resp.Body)

			// init stat response for current url
			response := StatResponse {
				Url:  u,
				Meta: StatMeta {
					Status:  resp.StatusCode,
					Headers: make(map[string]string),
				},
			}

			// populate stat response headers from response headers
			for name, value := range resp.Header {
				if len(value) > 0 {
					response.Meta.Headers[name] = string(value[0])
				}
			}

			var elements []StatElement

			elementsList := make(map[string]int)

			// create reader and tokenizer from requested html string
			reader := strings.NewReader(string(body[:]))
			tokenizer := html.NewTokenizer(reader)

			// Loop through tokens and calculate nodes map with count of each tag name
			loop: for {
				tokenType := tokenizer.Next()

				switch tokenType {
				case html.ErrorToken:
					break loop

				case html.DoctypeToken, html.StartTagToken, html.SelfClosingTagToken:
					tokenName, _ := tokenizer.TagName()

					tagName := string(tokenName)

					count, ok := elementsList[tagName]

					if ok {
						count++
					} else {
						count = 1
					}

					elementsList[tagName] = count
				}
			}

			// create StatElement elements for each tag
			for tagName, count := range elementsList {
				elements = append(elements, StatElement {
					TagName: tagName,
					Count:   count,
				})
			}

			response.Elements = elements

			channels <- response

			resp.Body.Close()
		}(u)
	}

	// populate output response used channels data
	for range validUrls {
		responses = append(responses, <-channels)
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(responses)
}