package main

// StatResponse type
// Used for representation result of single url info
type StatResponse struct {
	Url        string      `json:"url"`
	Meta       StatMeta    `json:"meta"`
	Elements []StatElement `json:"elements"`
}

// StatMeta type
// Used for representation meta information of single url
type StatMeta struct {
	Status  int               `json:"status"`
	Headers map[string]string `json:"headers"`
}

// StatElement type
// Used for representation information about single tag
type StatElement struct {
	TagName string `json:"tag-name"`
	Count   int    `json:"count"`
}